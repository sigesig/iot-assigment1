# IoT Assigment1

For this project we have decided to use docker for our database. In order to not have an in memory database.
The whole system runs through Docker. So **Docker** is needed to run this!!

_https://phoenixnap.com/kb/docker-on-raspberry-pi_

To run the program, run the following command from the **root** folder:

_sudo docker-compose up -d_

This will start the web server on _ip_of_raspberry:8080_

_ip_of_raspberry_ can be found from the terminal by running **ifconfig** it is the ip that starts with 192.168.

You can also use postman to access the RESTful API interface from the same ip and port.

Issues that we know of:
- Future iteration change docker image to python image, so the image isn't as big