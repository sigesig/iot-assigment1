let temperature_data = []
let humidity_data = []
let distance_data = []
let led_data = []

document.getElementById('next_chart_arrow').onclick = next_chart
document.getElementById('previous_chart_arrow').onclick = previous_chart
document.getElementById('change_led_state_button').onclick = change_led_state

get_data()
parse_data()
let current_chart_id = 1

google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawChart);

function update_humidity(humidity) {
    var text = document.getElementById("humidity-data")
    text.innerHTML = String(humidity)
}

function update_temperature(temperature) {
    var text = document.getElementById("temperature-data")
    text.innerHTML = String(temperature)
}

function update_distance(distance) {
    var text = document.getElementById("ultrasonic-data")
    text.innerHTML = String(distance)
}

function update_led_state(state) {
    var text = document.getElementById("led-data")
    state = String(state).toLowerCase()
    console.log(state)
    if (state === 'true') {
        text.innerHTML = 'On'
    } else {
        text.innerHTML = 'Off'
    }
}

function change_led_state() {
    fetch('/current/led', {
        method: 'PUT'
    })
        .then(response => response.json())
        .then(data => {
            console.log(data)
            update_led_state(String(data.value))

        })
}

function drawChart() {
    var data = new google.visualization.DataTable();
    let y_axis = ""
    data.addColumn('datetime', 'Time');

    switch(current_chart_id){
        case 0:
            y_axis = "Temperature (C°)"
            data.addColumn('number', y_axis);
            var rows = []
            for(var i = 0; i < temperature_data.length; i++) {
                var timestamp = temperature_data[i].timestamp.replace(' ', 'T')
                timestamp += '+0200'

                rows.push([new Date(timestamp), temperature_data[i].value])
            }
            data.addRows(rows)
        break;
        case 1:
            y_axis = "Humidity (%)"
            data.addColumn('number', y_axis);
            var rows = []
            for(var i = 0; i < humidity_data.length; i++) {
                var timestamp = humidity_data[i].timestamp.replace(' ', 'T')
                timestamp += '+0200'
                rows.push([new Date(timestamp), humidity_data[i].value])
            }
            data.addRows(rows)
        break;
        case 2:
            y_axis = "LED (state)"
            data.addColumn('number', y_axis);
            var rows = []
            for(var i = 0; i < led_data.length; i++) {
                var timestamp = led_data[i].timestamp.replace(' ', 'T')
                timestamp += '+0200'
                var value = 0
                if (led_data[i].value == "True") {
                    value = 1
                }
                rows.push([new Date(timestamp), value])
            }
            data.addRows(rows)
        break;
        case 3:
            y_axis = "Distance (cm)"
            data.addColumn('number', y_axis);
            var rows = []
            for(var i = 0; i < distance_data.length; i++) {
                var timestamp = distance_data[i].timestamp.replace(' ', 'T')
                timestamp += '+0200'
                rows.push([new Date(timestamp), distance_data[i].value])
            }
            data.addRows(rows)
        break;
    }

    var options = {
        hAxis: {
            direction:-1,
            slantedText:true,
            title: 'Time',
            textStyle: {
                color: '#000000',
                fontSize: 18,
                fontName: 'Arial',
                bold: false,
            },
            titleTextStyle: {
                color: '#000000',
                fontSize: 20,
                bold: true,
            }
        },
        vAxis: {
            title: y_axis,
            textStyle: {
                color: '#000000',
                fontSize: 24,
                bold: true
            },
            titleTextStyle: {
                color: '#000000',
                fontSize: 20,
                bold: false
            }
        },
        colors: ['#9C2415', '#097138']
    };
    var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
    chart.draw(data, options);
}

function previous_chart() {
    if (current_chart_id === 0) {
        current_chart_id = 3
    } else {
        current_chart_id--
    }
    drawChart()
}

function next_chart() {
    if (current_chart_id === 3) {
        current_chart_id = 0
    } else {
        current_chart_id++
    }
    drawChart()
}

function update_historic_data(temperature, humidity, distance, led) {
    temperature_data.push(temperature)
    humidity_data.push(humidity)
    led_data.push(led)
    distance_data.push(distance)
    drawChart()
}

async function parse_data() {
    let temperature = ""
    let humidity = ""
    let distance = ""
    let led = ""
    await fetch('/current/temperature')
        .then(response => response.json())
        .then(data => {
            console.log(data)
            update_temperature(data.value)
            temperature = data
        })
    await fetch('/current/humidity')
        .then(response => response.json())
        .then(data => {
            console.log(data)
            update_humidity(data.value)
            humidity = data
        })
    await fetch('/current/distance')
        .then(response => response.json())
        .then(data => {
            console.log(data)
            update_distance(data.value)
            distance = data
        })
    await fetch('/current/led')
        .then(response => response.json())
        .then(data => {
            console.log(data)
            update_led_state(data.value)
            led = data
        })
    update_historic_data(temperature, humidity, distance, led)
    setTimeout(parse_data, 5000);
}

function get_data() {
    var temperature_url =   '/historical/temperature/'
    var humidity_url =      '/historical/humidity/'
    var distance_url =      '/historical/distance/'
    var led_url =           '/historical/led/'

    // fuck dates, all my homies hate dates
    var date = new Date()
    date.setHours(date.getHours() + 1)
    var timestamp = date.toISOString()
    timestamp = timestamp.replace('T', ' ')
    timestamp = timestamp.slice(0, timestamp.length - 5)

    // fetch them dataz
    fetch(temperature_url + timestamp)
        .then(response => response.json())
        .then(data => {
            console.log('fetched temperature')
            temperature_data = data
            console.log(temperature_data)
        })

    fetch(humidity_url + timestamp)
        .then(response => response.json())
        .then(data => {
            console.log('fetched humidity')
            humidity_data = data
            console.log(humidity_data)
        })

    fetch(distance_url + timestamp)
        .then(response => response.json())
        .then(data => {
            console.log('fetched distance')
            distance_data = data
            console.log(distance_data)
        })

    fetch(led_url + timestamp)
        .then(response => response.json())
        .then(data => {
            console.log('fetched led')
            led_data = data
            console.log(led_data)
        })
}