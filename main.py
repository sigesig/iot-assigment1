from flask import Flask
from flask_restful import Api
import endpoint
from threading import Thread
import sensor
from database.session import engine, Session
from database.table import Base
from database import crud
from endpoint import update_measurements
from sqlalchemy.schema import CreateSchema

if __name__ == '__main__':
    # create database tables
    session = Session()
    if not engine.dialect.has_schema(engine, 'sensor'):
        engine.execute(CreateSchema('sensor'))
    Base.metadata.create_all(engine)

    # setup api
    app = Flask(__name__)
    api = Api(app)

    api.add_resource(endpoint.Index, '/')
    # current measurements endpoints
    api.add_resource(endpoint.Temperature, '/current/temperature')
    api.add_resource(endpoint.Humidity, '/current/humidity')
    api.add_resource(endpoint.Distance, '/current/distance')
    api.add_resource(endpoint.LED, '/current/led')

    # historical measurements endpoints
    api.add_resource(endpoint.HistoricalTemperature, '/historical/temperature/<string:timestamp>', '/historical/temperature')
    api.add_resource(endpoint.HistoricalHumidity, '/historical/humidity/<string:timestamp>', '/historical/humidity')
    api.add_resource(endpoint.HistoricalDistance, '/historical/distance/<string:timestamp>', '/historical/distance')
    api.add_resource(endpoint.HistoricalLED, '/historical/led/<string:timestamp>', '/historical/led')

    # setup thread to periodically read measurements
    thread = Thread(target=update_measurements)
    thread.start()

    app.run(debug=True, port=8080, use_reloader=False, host="0.0.0.0")
