from random import randint
import RPi.GPIO as GPIO
import adafruit_dht
import board
import time
from datetime import datetime

dht11 = adafruit_dht.DHT11(board.D12)

def get_temperature():
    got_measurement = False
    while not got_measurement:
        try:
            temperature = dht11.temperature
            if temperature is None:
                continue
            data = {
                "timestamp": datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
                "value": temperature,
                'unit' : 'celsius'
            }
            return data
        except Exception as err:
            # Reading doesn't always work! Just print error and we'll try again
            print("Reading from DHT failure: ", err.args)
    return data

def get_humidity():
    got_measurement = False
    while not got_measurement:
        try:
            humidity = dht11.humidity
            if humidity is None:
                continue
            data = {
                "timestamp": datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
                "value": humidity,
                'unit' : 'percent'
            }
            got_measurement = True
        except RuntimeError as err:
            # Reading doesn't always work! Just print error and we'll try again
            print("Reading from DHT failure: ", err.args)
    return data

def get_led_state():
    # setup LED
    GPIO.setmode(GPIO.BCM)
    LED = 4
    GPIO.setup(LED, GPIO.OUT)
    state = GPIO.input(LED)
    data = {
        "timestamp": datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
        "value": True if state == 1 else False,
        'unit' : 'True=On, False=Off'
    }
    return data

def set_led_state():
    # setup LED
    GPIO.setmode(GPIO.BCM)
    LED = 4
    GPIO.setup(LED, GPIO.OUT)
    state = GPIO.HIGH if GPIO.input(LED) == 0 else GPIO.LOW
    GPIO.output(LED, state)

def get_distance():
    # GPIO Mode (BOARD / BCM)
    GPIO.setmode(GPIO.BCM)

    # set GPIO Pins
    TRIG = 23
    ECHO = 24

    # set GPIO direction (IN / OUT)
    GPIO.setup(TRIG, GPIO.OUT)
    GPIO.setup(ECHO, GPIO.IN)
    GPIO.output(TRIG, False)
    time.sleep(2)

    GPIO.output(TRIG, True)
    time.sleep(0.00001)
    GPIO.output(TRIG, False)

    while GPIO.input(ECHO) == 0:
        pulse_start = time.time()

    while GPIO.input(ECHO) == 1:
        pulse_end = time.time()

    pulse_duration = pulse_end - pulse_start
    distance = pulse_duration * 17150
    distance = round(distance, 2)
    GPIO.cleanup()

    time_distance = {
        "timestamp": datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
        "value": distance,
        'unit' : 'cm'
    }
    return time_distance


#########################################
#            MOCK SENSORS               #
#########################################
def mock_temperature():
    
    return {
        'value': randint(0, 100),
        'timestamp': datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
        'unit' : 'celsius'
    }

def mock_humidity():
    return {
        'value': randint(0, 100),
        'timestamp': datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
        'unit' : 'percent'
    }

def mock_distance():
    return {
        'value': randint(0, 100),
        'timestamp': datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
        'unit' : 'cm'
    }

def mock_led():
    return {
        'value': True if randint(0, 100) > 50 else False,
        'timestamp': datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
        'unit' : 'True=On, False=Off'
    }