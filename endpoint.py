from flask import render_template, make_response, request
from flask_restful import Resource
import sensor
from database import crud
import json
from time import sleep
from datetime import datetime

class Index(Resource):
    def get(self):
        content_type = request.headers.get('Content-Type')
        if content_type == 'application/json':
            response = { # this was fun to write, maybe there is a smarter way
                'title' : 'Welcome to Victor\'s room! Here, you can see the temperature, humidity and distance, as well as see and control an LED!',
                'endpoints' : {
                    'current' : {
                        'temperature'   : {
                            'endpoint'      : '/current/temperature',
                            'description'   : 'Read the current temperature.',
                            'methods'       : 'GET'
                        },
                        'humidity'      : {
                            'endpoint'      : '/current/humidity',
                            'description'   : 'Read the current humidity.',
                            'methods'       : 'GET'
                        },
                        'distance'      : {
                            'endpoint'      : '/current/distance',
                            'description'   : 'Read the current distance.',
                            'methods'       :  'GET'
                        },
                        'led'           : {
                            'endpoint'      : '/current/led',
                            'description'   : 'Read or set the current state of the LED.',
                            'methods'       : 'GET, PUT'
                        }
                    },
                    'historical' : {
                        'temperature'   : {
                            'endpoint'      : '/historical/temperature',
                            'description'   : 'Read historical data on temperature.',
                            'methods'       : 'GET',
                            'parameters'    : {
                                'timestamp' : 'Returns measurements since the timestamp. Valid formats are [YYYY-MM-dd HH:mm:ss] or [YYYY-MM-dd]. If no timestamp is specified, returns all historical measurements.'
                            }
                        },
                        'humidity'      : {
                            'endpoint'      : '/historical/humidity',
                            'description'   : 'Read historical data on humidity.',
                            'methods'       : 'GET',
                            'parameters'    : {
                                'timestamp' : 'Returns measurements since the timestamp. Valid formats are [YYYY-MM-dd HH:mm:ss] or [YYYY-MM-dd]. If no timestamp is specified, returns all historical measurements.'
                            }
                        },
                        'distance'      : {
                            'endpoint'      : '/historical/distance',
                            'description'   : 'Read historical data on distance.',
                            'methods'       :  'GET',
                            'parameters'    : {
                                'timestamp' : 'Returns measurements since the timestamp. Valid formats are [YYYY-MM-dd HH:mm:ss] or [YYYY-MM-dd]. If no timestamp is specified, returns all historical measurements.'
                            }
                        },
                        'led'           : {
                            'endpoint'      : '/historical/led',
                            'description'   : 'Read historical data on the LED state.',
                            'methods'       : 'GET',
                            'parameters'    : {
                                'timestamp' : 'Returns measurements since the timestamp. Valid formats are [YYYY-MM-dd HH:mm:ss] or [YYYY-MM-dd]. If no timestamp is specified, returns all historical measurements.'
                            }
                        }
                    }
                }
            }
            return response
        else:
            headers = {'Content-Type': 'text/html'}
            return make_response(render_template('index.html'), 200, headers)

# current measurements endpoints
class Temperature(Resource):
    def get(self):
        return crud.get_latest_temperature()

class Humidity(Resource):
    def get(self):
        return crud.get_latest_humidity()

class Distance(Resource):
    def get(self):
        return crud.get_latest_distance()

class LED(Resource):
    def get(self):
        return crud.get_latest_led()

    def put(self):
        sensor.set_led_state()
        return sensor.get_led_state()

# helper function to validate date strings
def validate_timestamp(timestamp, format):
    try:
        datetime.strptime(timestamp, format)
    except:
        return False
    return True

# historical measurements endpoints
class HistoricalTemperature(Resource):
    def get(self, timestamp=None):
        # validate timestamp
        if timestamp:
            if not (validate_timestamp(timestamp, '%Y-%m-%d %H:%M:%S') or validate_timestamp(timestamp, '%Y-%m-%d')):
                return 'Invalid timestamp: {0}. Valid formats are [YYYY-MM-dd HH:mm:ss] or [YYYY-MM-dd]'.format(timestamp), 400
        return crud.get_historical_temperature(timestamp)

class HistoricalHumidity(Resource):
    def get(self, timestamp=None):
        # validate timestamp
        if timestamp:
            if not (validate_timestamp(timestamp, '%Y-%m-%d %H:%M:%S') or validate_timestamp(timestamp, '%Y-%m-%d')):
                return 'Invalid timestamp: {0}. Valid formats are [YYYY-MM-dd HH:mm:ss] or [YYYY-MM-dd]'.format(timestamp), 400
        return crud.get_historical_humidity(timestamp)

class HistoricalDistance(Resource):
    def get(self, timestamp=None):
        # validate timestamp
        if timestamp:
            if not (validate_timestamp(timestamp, '%Y-%m-%d %H:%M:%S') or validate_timestamp(timestamp, '%Y-%m-%d')):
                return 'Invalid timestamp: {0}. Valid formats are [YYYY-MM-dd HH:mm:ss] or [YYYY-MM-dd]'.format(timestamp), 400
        return crud.get_historical_distance(timestamp)

class HistoricalLED(Resource):
    def get(self, timestamp=None):
        # validate timestamp
        if timestamp:
            if not (validate_timestamp(timestamp, '%Y-%m-%d %H:%M:%S') or validate_timestamp(timestamp, '%Y-%m-%d')):
                return 'Invalid timestamp: {0}. Valid formats are [YYYY-MM-dd HH:mm:ss] or [YYYY-MM-dd]'.format(timestamp), 400
        return crud.get_historical_led(timestamp)

def update_measurements():
    while True:
        temperature = sensor.get_temperature()
        crud.write_latest_temperature(temperature['value'], temperature['timestamp'])
        humidity = sensor.get_humidity()
        crud.write_latest_humidity(humidity['value'], humidity['timestamp'])
        distance = sensor.get_distance()
        crud.write_latest_distance(distance['value'], distance['timestamp'])
        led = sensor.get_led_state()
        crud.write_latest_led(led['value'], led['timestamp'])
        print('writing new values to database')
        sleep(5)
