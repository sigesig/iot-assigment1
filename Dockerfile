# syntax=docker/dockerfile:1
FROM ubuntu:20.04
WORKDIR /
ARG DEBIAN_FRONTEND=noninteractive
# install nodejs, libgpiod2, python
RUN apt-get update
RUN apt-get -y install python3
RUN apt-get -y install python3-pip
RUN apt-get -y install npm
RUN apt-get -y install nodejs
RUN apt-get -y install libgpiod2
RUN apt-get -y install libpq-dev

# install python dependencies
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

# install node dependencies
COPY package*.json ./
RUN npm config set registry http://registry.npmjs.org/  
RUN npm install

EXPOSE 8080
COPY . .
CMD ["python3", "main.py"]
