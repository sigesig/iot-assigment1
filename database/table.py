from sqlalchemy.orm import declarative_base
from sqlalchemy import Column, types

Base = declarative_base()

class Distance(Base):
    __tablename__ = 'distance'
    __table_args__ = {'schema': 'sensor'}

    id = Column(types.Integer, primary_key=True)
    value = Column(types.Numeric)
    timestamp = Column(types.DateTime)

class Humidity(Base):
    __tablename__ = 'humidity'
    __table_args__ = {'schema': 'sensor'}

    id = Column(types.Integer, primary_key=True)
    value = Column(types.Numeric)
    timestamp = Column(types.DateTime)

class Temperature(Base):
    __tablename__ = 'temperature'
    __table_args__ = {'schema': 'sensor'}

    id = Column(types.Integer, primary_key=True)
    value = Column(types.Numeric)
    timestamp = Column(types.DateTime)

class LED(Base):
    __tablename__ = 'led'
    __table_args__ = {'schema': 'sensor'}

    id = Column(types.Integer, primary_key=True)
    value = Column(types.Boolean)
    timestamp = Column(types.TIMESTAMP)