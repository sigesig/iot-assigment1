from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from time import sleep

engine = None
Session = None

while True:
    try:
        engine = create_engine('postgresql://admin:password@database/iot')
        engine.execute('SELECT 1')
        Session = sessionmaker(engine)
    except:
        print('Waiting for database to start..')
        sleep(1)
    else:
        break