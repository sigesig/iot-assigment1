from database.session import Session
from sqlalchemy import select
from database import table
from datetime import datetime
import json
from sqlalchemy import func

# get historical measurements from database
def get_historical_temperature(timestamp):
    if timestamp:
        stmt = select(table.Temperature).where(table.Temperature.timestamp >= timestamp)
    else:
        stmt = select(table.Temperature)
    with Session() as session:
        data = session.execute(stmt)
        res = []
        for row in data:
            res.append({
                'value'     : float(row[0].value),
                'timestamp' : row[0].timestamp.strftime('%Y-%m-%d %H:%M:%S'),
                'unit'      : 'celsius'
            })
        return res

def get_historical_humidity(timestamp):
    if timestamp:
        stmt = select(table.Humidity).where(table.Humidity.timestamp >= timestamp)
    else:
        stmt = select(table.Humidity)
    with Session() as session:
        data = session.execute(stmt)
        res = []
        for row in data:
            res.append({
                'value'     : float(row[0].value),
                'timestamp' : row[0].timestamp.strftime('%Y-%m-%d %H:%M:%S'),
                'unit'      : 'percent'
            })
        return res

def get_historical_distance(timestamp):
    if timestamp:
        stmt = select(table.Distance).where(table.Distance.timestamp >= timestamp)
    else:
        stmt = select(table.Distance)
    with Session() as session:
        data = session.execute(stmt)
        res = []
        for row in data:
            res.append({
                'value'     : float(row[0].value),
                'timestamp' : row[0].timestamp.strftime('%Y-%m-%d %H:%M:%S'),
                'unit'      : 'cm'
            })
        return res

def get_historical_led(timestamp):
    if timestamp:
        stmt = select(table.LED).where(table.LED.timestamp >= timestamp)
    else:
        stmt = select(table.LED)
    with Session() as session:
        data = session.execute(stmt)
        res = []
        for row in data:
            res.append({
                'value'     : str(bool(row[0].value)),
                'timestamp' : row[0].timestamp.strftime('%Y-%m-%d %H:%M:%S'),
                'unit'      : 'True = on, False = off'
            })
        return res

# get latest measurements from database
def get_latest_temperature():
    stmt = select(table.Temperature).order_by(table.Temperature.id.desc())
    with Session() as session:
        data = session.execute(stmt).fetchone()
        res = {
            'value'         : float(data[0].value),
            'timestamp'     : data[0].timestamp.strftime('%Y-%m-%d %H:%M:%S'),
            'unit'          : 'celsius'
        }
        return res

def get_latest_humidity():
    stmt = select(table.Humidity).order_by(table.Humidity.id.desc())
    with Session() as session:
        data = session.execute(stmt).fetchone()
        res = {
            'value'         : float(data[0].value),
            'timestamp'     : data[0].timestamp.strftime('%Y-%m-%d %H:%M:%S'),
            'unit'          : 'percent'
        }
        return res

def get_latest_distance():
    stmt = select(table.Distance).order_by(table.Distance.id.desc())
    with Session() as session:
        data = session.execute(stmt).fetchone()
        res = {
            'value'         : float(data[0].value),
            'timestamp'     : data[0].timestamp.strftime('%Y-%m-%d %H:%M:%S'),
            'unit'          : 'cm'
        }
        return res

def get_latest_led():
    stmt = select(table.LED).order_by(table.LED.id.desc())
    with Session() as session:
        data = session.execute(stmt).fetchone()
        res = {
            'value'         : str(bool(data[0].value)),
            'timestamp'     : data[0].timestamp.strftime('%Y-%m-%d %H:%M:%S'),
            'unit'          : 'cm'
        }
        return res

# write latest measurements to database
def write_latest_temperature(value, timestamp=None):
    if not timestamp:
        timestamp = datetime.now()
    data = table.Temperature(timestamp=timestamp, value=value)
    with Session() as session:
        session.add(data)
        session.commit()

def write_latest_humidity(value, timestamp=None):
    if not timestamp:
        timestamp = datetime.now()
    data = table.Humidity(timestamp=timestamp, value=value)
    with Session() as session:
        session.add(data)
        session.commit()

def write_latest_distance(value, timestamp=None):
    if not timestamp:
        timestamp = datetime.now()
    data = table.Distance(timestamp=timestamp, value=value)
    with Session() as session:
        session.add(data)
        session.commit()

def write_latest_led(value, timestamp=None):
    if not timestamp:
        timestamp = datetime.now()
    data = table.LED(timestamp=timestamp, value=value)
    with Session() as session:
        session.add(data)
        session.commit()